# Hi I'm [JYOTIRMOY✨](https://linktr.ee/jyotirmoybarman)
<img src="pic/banner.png">
<p align="center"> <img src="https://komarev.com/ghpvc/?username=jyotirmoybarman&label=Views&color=blue&style=plastic" alt="jyotirmoybarman" /> <img src="https://img.shields.io/github/followers/jyotirmoybarman?logo=github&style=plastic"><a href="https://instagram.com/jyotirmoy_barman_"> <img src="https://img.shields.io/badge/Instagram-jyotirmoy__barman__-blue?logo=instagram&logoColor=white&style=plastic"></a><br/> </p>

## ᴄᴏɴᴛᴇɴᴛ

- [ᴀʙᴏᴜᴛ ᴍᴇ](https://github.com/jyotirmoybarman/jyotirmoybarman#ᴀʙᴏᴜᴛ-ᴍᴇ)
- [ᴡᴏʀᴋsᴘᴀᴄᴇ](https://github.com/jyotirmoybarman/jyotirmoybarman#ᴡᴏʀᴋsᴘᴀᴄᴇ)
- [ʜᴏʙʙɪᴇs](https://github.com/jyotirmoybarman/jyotirmoybarman#ʜᴏʙʙɪᴇs)
- [ʀᴇsᴜᴍᴇ](https://github.com/jyotirmoybarman/jyotirmoybarman#ʀᴇsᴜᴍᴇ)
- [ᴇxᴘᴇʀɪᴇɴᴄᴇ ᴀɴᴅ ᴛᴏᴏʟs](https://github.com/jyotirmoybarman/jyotirmoybarman#ᴇxᴘᴇʀɪᴇɴᴄᴇ-ᴀɴᴅ-ᴛᴏᴏʟs-)
- [ɢɪᴛʜᴜʙ sᴛᴀᴛs](https://github.com/jyotirmoybarman/jyotirmoybarman#ɢɪᴛʜᴜʙ-sᴛᴀᴛs)
- [ʀᴇᴘᴏsɪᴛᴏʀʏ](https://github.com/jyotirmoybarman/jyotirmoybarman#ʀᴇᴘᴏsɪᴛᴏʀʏ)
- [ғɪɴᴅ ᴍᴇ ᴏɴ ᴛʜᴇ ᴡᴇʙ](https://github.com/jyotirmoybarman/jyotirmoybarman#ғɪɴᴅ-ᴍᴇ-ᴏɴ-ᴛʜᴇ-ᴡᴇʙ)
- [ᴅᴏɴᴀᴛᴇ](https://github.com/jyotirmoybarman/jyotirmoybarman#ᴅᴏɴᴀᴛᴇ)

## ᴀʙᴏᴜᴛ ᴍᴇ

<kbd>

![boy](pic/details.gif)

</kbd>

### ᴡᴏʀᴋsᴘᴀᴄᴇ

<p align="center">
<img src="https://img.shields.io/badge/redmi_note_8_pro-E50914?style=for-the-badge&logo=Xiaomi&color=orange&logoColor=white"> 
<img src="https://img.shields.io/badge/Termux-E50914?style=for-the-badge&logo=linux&color=black&logoColor=white">   <img src="https://img.shields.io/badge/VIM-%2311AB00.svg?&style=for-the-badge&logo=vim&logoColor=white">
<img src="https://img.shields.io/badge/git-red?style=for-the-badge&logo=git&logoColor=white">
<img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white">
</p>

### ʜᴏʙʙɪᴇs

- 📷 ᴘʜᴏᴛᴏɢʀᴀᴘʜʏ 
- 👨‍💻 ᴘʀᴏɢʀᴀᴍᴍɪɴɢ
- 🖋️ ᴡʀɪᴛɪɴɢ ᴀʀᴛɪᴄʟᴇs
- 🔎 ᴇxᴘʟᴏʀɪɴɢ

### ʀᴇsᴜᴍᴇ

<p align="left">
<a href=""><img src="https://img.shields.io/badge/-_ᴅᴏᴡɴʟᴏᴀᴅ_-green?style=plastic"></a> <a href=""><img src="https://img.shields.io/badge/-_ᴠɪᴇᴡ_-red?style=plastic"></a> <a href=""><img src="https://img.shields.io/badge/-_sʜᴀʀᴇ_-blue?style=plastic"></a>
</p>

## ᴇxᴘᴇʀɪᴇɴᴄᴇ ᴀɴᴅ ᴛᴏᴏʟs [∞](Experience.md)

<img src="pic/linux.png" width=40px height=40px><img src="pic/Cpp.png" width=40px height=40px><img src="pic/python.png" width=40px height=40px> <img src="pic/mysql.png" width=60px height=40px>
<img src="pic/git.png" width=40px height=40px> <img src="pic/vim.png" width=40px height=40px> <img src="pic/termux.png" width=40px height=40px>   <img src="pic/GitHub.png" width=40px height=40px>
> Click to Infinity ∞ to view in detail.

## ɢɪᴛʜᴜʙ sᴛᴀᴛs 

<img src="https://github-readme-stats.vercel.app/api?username=jyotirmoybarman&border_radius=30&show_icons=false&theme=github_darkb&line_height=25&hide_border=true&bg_color=90,0098f7ff,464bffff&title_color=fff&text_color=fff"><img src="https://github-readme-stats.vercel.app/api/top-langs/?username=jyotirmoybarman&border_radius=30&layout=compact&hide_border=true&bg_color=90,0098f7ff,464bffff&title_color=fff&text_color=fff"> 

## ʀᴇᴘᴏsɪᴛᴏʀʏ

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=jyotirmoybarman&repo=Cpp-Language&border_radius=30&bg_color=90,0098f7ff,464bffff&title_color=fff&text_color=fff&layout=compact&hide_border=true)](https://github.com/jyotirmoybarman/Cpp-Language)
[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=jyotirmoybarman&repo=Python-3-Language&border_radius=30&bg_color=90,0098f7ff,464bffff&title_color=fff&text_color=fff&layout=compact&hide_border=true)](https://github.com/jyotirmoybarman/Cpp-Language)

## ғɪɴᴅ ᴍᴇ ᴏɴ ᴛʜᴇ ᴡᴇʙ 
- Learning in public [Twitter](https://twitter.com/jyotirmoytweet?s=08) 🎥
- Creating a network all over the world [LinkedIn](https://www.linkedin.com/in/jyotirmoy-barman-275018203) 🕸️
- Sharing some Knowledge by my [Articles](https://jyotirmoy.hashnode.dev/) 📰
- Hangout with me in [Instagram](https://instagram.com/jyotirmoy_barman_) 🎮
<p align="center">
<a href="https://github.com/jyotirmoybarman/">
        <img src="https://img.shields.io/badge/-_jyotirmoybarman_-grey?style=plastic&logo=github">
</a>
<a href="https://instagram.com/jyotirmoy_barman_">
        <img src="https://img.shields.io/badge/-_jyotirmoy__barman__-red?style=plastic&logo=instagram&logoColor=white">
</a>
<a href="https://www.linkedin.com/in/jyotirmoy-barman-275018203">
        <img src="https://img.shields.io/badge/-_Jyotirmoy_Barman_-blue?style=plastic&logo=linkedin">
</a>
<a href="https://linktr.ee/jyotirmoybarman">
        <img src="https://img.shields.io/badge/-_jyotirmoybarman_-green?style=plastic&logo=linktree&logoColor=white">
</a>
</p>

## ᴅᴏɴᴀᴛᴇ

- 𝚂𝚞𝚙𝚙𝚘𝚛𝚝 𝙼𝚎 𝙱𝚢 𝙳𝚘𝚗𝚊𝚝𝚒𝚗𝚐 💰

<p align="center">
<img src="https://img.shields.io/badge/-ʙᴜʏ_ᴍᴇ_ᴀ_ᴄᴏғғᴇᴇ-yellow?style=plastic&logo=buy-me-a-coffee&logoColor=white"> <img src="https://img.shields.io/badge/-ᴘʜᴏɴᴇᴘᴇ-purple?style=plastic&logo=phonepe&logoColor=white"> <img src="https://img.shields.io/badge/-_ᴇᴛʜᴇʀᴇᴜᴍ_-3C3C3D?style=plastic&logo=Ethereum"> <img src="https://img.shields.io/badge/_ʙɪᴛᴄᴏɪɴ_-000000?style=plastic&logo=bitcoin&logoColor=white"> <img src="https://img.shields.io/badge/-🏦_ʙᴀɴᴋ_-blue?style=plastic&logo=&logoColor=white">
</p>
